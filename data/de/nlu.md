## intent:acknowledge
- ok
- ich hab's
- verstanden
- alles klar
- k

## intent:affirm
- ja
- tatsächlich
- na sicher
- das klingt gut
- richtig

## intent:bot_welcome
- bot_welcome

## intent:coffee
- kaffee
- ich will kaffee
- kaffee trinken
- ich hätte gern eine tasse kaffee

## intent:coffee_type
- ich möchte [latte](coffee_type)
- ich hätte gern [americano](coffee_type)
- einen [macchiato](coffee_type), danke schön
- einen [pour over](coffee_type), danke
- einen [americano](coffee_type) vielen dank

## intent:deny
- nein
- noch nie
- das glaube ich nicht
- mag das nicht
- auf keinen fall
- nicht wirklich

## intent:goodbye
- tschüss
- auf wiedersehen
- bis demnächst
- bis später

## intent:greet
- hallo
- guten morgen
- guten abend
- sie da

## intent:mood_great
- perfekt
- sehr gut
- großartig
- toll
- wunderbar
- ich fühle mich sehr gut
- ich bin großartig
- mir geht's gut
- ich bin gut
- es geht mir gut
- ich bin ok
- alles gut

## intent:mood_unhappy
- traurig
- sehr traurig
- unglücklich
- Schlecht
- sehr schlecht
- schrecklich
- furchtbar
- nicht sehr gut
- extrem traurig
- so traurig
- schrecklich

## intent:opinion+negative
- ugh
- das ist scheiße
- woah
- blöd
- das ist falsch
- ach nein!

## intent:opinion+positive
- nett!
- ausgezeichnet
- genial
- haha
- danke
- vielen dank

## synonym:americano
- long black
- american coffee
- italiano

## synonym:latte
- cafe au lait
- caffelatte
- milchkaffee

## lookup:coffee_types
- americano
- cappuccino
- chai
- decaf
- doppio
- double
- half-caff
- latte
- macchiato
- mocha
- pour over
- brewed coffee
- iced
- matcha latte

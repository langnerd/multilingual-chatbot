## intent:acknowledge
- ok
- got it
- understood
- k

## intent:affirm
- yes
- indeed
- of course
- that sounds good
- correct

## intent:bot_welcome
- bot_welcome

## intent:coffee
- coffee
- i want coffee
- coffee time
- can i get coffee please?
- coffee plz

## intent:coffee_type
- i want [latte](coffee_type)
- can i get [americano](coffee_type) please
- a [macchiato](coffee_type), thank you
- a [pour over](coffee_type), please
- [americano](coffee_type) please

## intent:deny
- no
- never
- I don't think so
- don't like that
- no way
- not really

## intent:goodbye
- bye
- goodbye
- see you around
- see you later

## intent:greet
- hey
- hello
- hi
- good morning
- good evening
- hey there

## intent:mood_great
- perfect
- very good
- great
- amazing
- wonderful
- i am feeling very good
- i am great
- i'm good
- i am good
- i'm fine
- i am fine
- just fine

## intent:mood_unhappy
- sad
- very sad
- unhappy
- bad
- very bad
- awful
- terrible
- not very good
- extremely sad
- so sad
- awful

## intent:opinion+negative
- ugh
- that sucks
- woah
- stupid
- that's wrong
- oh no!

## intent:opinion+positive
- nice!
- excellent
- awesome
- haha
- thanks
- thank you
- thx
- ty
- ty

## synonym:americano
- long black
- american coffee
- italiano

## synonym:latte
- cafe au lait
- caffelatte
- milchkaffee

## lookup:coffee_types
- americano
- cappuccino
- chai
- decaf
- doppio
- double
- half-caff
- latte
- macchiato
- mocha
- pour over
- brewed coffee
- iced
- matcha latte

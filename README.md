# Multilingual Chatbot
This is an example of a conversational bot powered by the [Rasa] framework.

## Demo
![](./docs/chatbot_demo.gif)


## Features
* Uses the latest stable version of Rasa
* Fully dockerized, no tedious setup required
* Currently bi-lingual, it's easy to add support for a new (human) language
* Makes it easy to retrain the model or run interactive learning


## Prerequisites
* Docker
* Docker Compose
* git

## How to Build and Run

Just clone the repo and start all services by running `./server.sh start`

Then visit `http://localhost:8080` and have fun chatting away.

Beware that the very first time the start might take a long time (several minutes). Once Docker images and all dependencies are downloaded and installed the app will start much quicker. 

## Usage Details

`server.sh` can be run with the following options.

```
  start             .. starts all services. Once done, visit http://localhost:8080
  stop              .. stops all services and discards docker containers
  train [locale]    .. (re)trains the models using stories and nlu data.
                       takes an optional argument - a locale, currently *en* or *de*, to retrain a selected model
                       when no arguments are passed, all models will be retrained
  interactive       .. runs an interactive learning session, where you can help the bot learn to correctly recognize intents
```

## Tech Stack
* Docker
* Python 3.6, Flask
* Rasa 1.1.7
* [Rasa WebChat](https://github.com/mrbot-ai/rasa-webchat)


## Credits:
* [Building a multi-lingual chatbot using Rasa and Chatfuel](https://blog.usejournal.com/building-a-multi-lingual-chatbot-using-rasa-and-chatfuel-cca20cbc645a)
* [NLP behind Chatbots](https://medium.com/bhavaniravi/demystifying-rasa-nlu-1-training-91a08429c9fb)
* [Bot Avatar](https://pixabay.com/illustrations/bot-icon-robot-automated-cyborg-2883144/)
* [CSS language dropdown](https://codepen.io/axel1998/pen/RWOpXq)
* Coffee mug - Image by <a href="https://pixabay.com/users/Clker-Free-Vector-Images-3736/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=34251">Clker-Free-Vector-Images</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=34251">Pixabay</a>
* Coffee mug - Image by <a href="https://pixabay.com/users/OpenClipart-Vectors-30363/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=156144">OpenClipart-Vectors</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=156144">Pixabay</a>

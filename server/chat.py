import os
import asyncio
import sys, getopt
from pathlib import Path
from rasa.core.channels.socketio import SocketIOInput
from rasa.core.agent import Agent
from rasa.utils.endpoints import EndpointConfig


def __exists_model(path):
    return Path(path).is_file()

def main(argv):
    port = -1
    model = ''
    try:
        opts, args = getopt.getopt(argv, 'p:m:', ['port=', 'model='])
    except getopt.GetoptError as msg:
        print(msg)
        sys.exit(2)

    for opt, arg in opts:
        if opt in ('-p', '--port'):
            try:
                port = int(arg)
            except ValueError:
                print('port must be an integer!')
                sys.exit(2)

        elif opt in ('-m', '--model'):
            model = arg

    if port <= 0 or not model:
        print('chat.py --port=<port> --model=[path to your nlu model]')
        sys.exit(2)

    if not __exists_model(model):
        print('Model {} not found.'.format(model))
        sys.exit(2)

    agent = Agent.load(
        model,
        action_endpoint=EndpointConfig('http://action_server:5055/webhook')
    )

    input_channel = SocketIOInput(
    	# event name for messages sent from the user
    	user_message_evt="user_uttered",
    	# event name for messages sent from the bot
    	bot_message_evt="bot_uttered",
    	# socket.io namespace to use for the messages
    	namespace=None
    )

    loop = asyncio.get_event_loop()
    loop.run_until_complete(agent.handle_channels([input_channel], port))

if __name__ == '__main__':
    main(sys.argv[1:])

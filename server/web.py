from flask import Flask, render_template, send_from_directory

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/css/<path:path>')
def css(path):
    return send_from_directory('css', path)

@app.route('/images/<path:path>')
def images(path):
    return send_from_directory('images', path)

#if __name__ == '__main__':
#    app.run(host='0.0.0.0',port=8080)


#!/bin/bash
# set -x

# Make the script runnable from any directory
cd `dirname "$0"`

# Error handling
set -o errexit          # Exit on most errors
set -o errtrace         # Make sure any error trap is inherited
set -o pipefail         # Use last non-zero exit code in a pipeline

if [ ! -f "docker-compose.yml" ]; then
  echo "docker-compose.yml not found. Have you deleted it by any chance?"
  exit 1;
fi

LOCALES=( "en" "de" )

function start() {
  echo "Starting all services .."
  docker-compose up -d
  if [ $? -eq 0 ]; then
    echo "All done"
  else
    echo "One or more services failed to start. The bot might not work as expected."
  fi
}

function stop() {
  echo "Shutting down .."
  docker-compose down
  if [ $? -eq 0 ]; then
    echo "All done"
  else
    echo "One or more services failed to shut down. Check for running Docker containers and shut them down"
  fi
}

function train() {
  if [ -z "$1" ]; then
    for locale in "${LOCALES[@]}"; do
      train_locale "$locale"
    done
  else
    train_locale "$1"
  fi
}

function train_locale() {
  if [ -z "$1" ]; then
    echo "No locale specified, skipping training"
    exit 1
  fi

  config="config/$1/config.yml"
  domain="config/$1/domain.yml"
  data="data/$1"

  if [ ! -f "$config" ] || [ ! -f "$domain" ] || [ ! -d "$data" ]; then
    echo "Missing config and / or data for locale '$1'. Make sure to add them and try again."
    exit 1
  fi

  docker run -v $(pwd):/app \
    rasa/rasa:1.1.7-full train \
    --config "$config" \
    --domain "$domain" \
    --data "$data" \
    --out models \
    --fixed-model-name "chat-model-$1"
}

function interactive_learning() {
  start
  if [ $? -eq 0 ]; then
    echo "Starting interactive learning session .."
    docker-compose run rasa interactive --verbose -c config/en -m models/chat-model-en.tar.gz --endpoints endpoints.yml
    echo "All done, happy learning ;-)"
  else
    echo "Services failed to start, exiting .."
    exit 1
  fi

}

function show_help() {
    echo "Usage:"
    echo "  start             .. starts all services. Once done, visit http://localhost:8080"
    echo "  stop              .. stops all services and discards docker containers"
    echo "  train [locale]    .. (re)trains the models using stories and nlu data."
    echo "                       takes an optional argument - a locale, currently *en* or *de*, to retrain a selected model"
    echo "                       when no arguments are passed, all models will be retrained"
    echo "  interactive       .. runs an interactive learning session, where you can help the bot learn to correctly recognize intents"
}

case "$1" in
  -h | --help)
    show_help
    exit 0
    ;;
  "start")
    start
    ;;
  "stop")
    stop
    ;;
  "train")
    train "$2"
    ;;
  "interactive")
    interactive_learning
    ;;
esac
